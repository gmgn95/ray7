package com.mahmoud.mohammed.rayhtask.ui;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mahmoud.mohammed.rayhtask.utils.Constants;
import com.mahmoud.mohammed.rayhtask.R;
import com.mahmoud.mohammed.rayhtask.utils.Util;
import com.mahmoud.mohammed.rayhtask.model.Passenger;
import com.mahmoud.mohammed.rayhtask.model.Position;
import com.mahmoud.mohammed.rayhtask.model.Trip;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements RoutingListener, OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapLongClickListener {
    GoogleMap mMap;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    GoogleApiClient mGoogleApiClient;
    TextView place1tv;
    TextView place2tv;
    Button pickupbtn;
    ProgressDialog progressDialog;
    private List<Polyline> polylines;
    Button takecarbtn;
    ImageView mCurrentLocationIv;
    Location mLastLocation;
    double mCurrentLatitude, mCurrentLongLatitude;
    boolean isCurrentPosPressed = false, isDestinationtionPressed = false;
    Marker markerFrom, markerTo, mCurrentMarker;
    Passenger passenger;
    Position source, destination, mCurrentLocationPos;
    //  MapPresenter presenter;
    protected LatLng start;
    protected LatLng end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setTitle(null);
        place1tv = (TextView) findViewById(R.id.autocomplete_tv_from);
        place2tv = (TextView) findViewById(R.id.autocomplete_tv_to);
        pickupbtn = (Button) findViewById(R.id.pickup_btn);
        takecarbtn = (Button) findViewById(R.id.take_my_car_btn);
        mCurrentLocationIv = (ImageView) findViewById(R.id.imgMyLocation);
        passenger = new Passenger();
        mCurrentLocationPos = new Position();
        polylines = new ArrayList<>();
        registerGoogleApiClient();


        //select source
        place1tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                launchIntentBuilder(Constants.PLACE_AUTOCOMPLETE_FROM);
            }
        });
        //select destination
        place2tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check to prevent multiple click
                if (!isDestinationtionPressed) {
                    isDestinationtionPressed = true;
                    launchIntentBuilder(Constants.PLACE_AUTOCOMPLETE_TO);
                } else {
                    isDestinationtionPressed = false;
                    if (markerTo != null) {
                        markerTo.remove();
                    }
                    launchIntentBuilder(Constants.PLACE_AUTOCOMPLETE_TO);
                }
            }
        });

        mCurrentLocationIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.Operations.isOnline(MainActivity.this)) {
                    if (isCurrentPosPressed == true) {
                        isCurrentPosPressed = false;
                        mCurrentLocationIv.setImageResource(R.drawable.out);
                        mCurrentMarker.remove();
                        place1tv.setText("");
                    } else {
                        isCurrentPosPressed = true;
                        mCurrentLocationIv.setImageResource(R.drawable.track_my_location);
                        source = new Position();
                        source.setLatitude(mCurrentLatitude);
                        source.setLongLatitude(mCurrentLongLatitude);
                        passenger.setSourceCoordinates(source);
                        updateLocation();
                    }

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Check Connection please ", Toast.LENGTH_LONG)
                            .show();

                }


            }
        });

        fetchMapFragment();
    }

    public void pickUpTime(View view) {
        java.util.Calendar mcurrentTime = java.util.Calendar.getInstance();
        int hour = mcurrentTime.get(java.util.Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(java.util.Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                Trip trip = new Trip();
                //set time for the trip
                trip.setTime(selectedHour + "" + selectedMinute);
                passenger.setTrip(trip);

                String hour=selectedHour+"";
                String min=selectedMinute+"";
                if(selectedHour<10)
                {
                    hour="0"+selectedHour;

                }
                if(selectedMinute<10)
                {
                    min="0"+selectedMinute;
                }
                String FROM=hour + ":" + min;
                 pickupbtn.setText(FROM);
                Toast.makeText(MainActivity.this,hour + ":" + min,Toast.LENGTH_SHORT).show();



            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();


    }

    private void updateLocation() {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(passenger.getmSource(), Constants.DEFAULT_ZOOM);
        mMap.animateCamera(cameraUpdate);
        //check if passeanger select place from autocomplete list
        if (place1tv.getText().length() > 0) {
            //remove source location ,replace it with current location and update ui
            markerFrom.remove();
            place1tv.setText(null);
        }
        mCurrentMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())).title("It's Me!"));
        String Resault = Util.Operations.getAddressName(mLastLocation.getLatitude(), mLastLocation.getLongitude(), this);
        place1tv.setText(Resault);
    }

    private void registerGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addConnectionCallbacks(MainActivity.this)
                    .addOnConnectionFailedListener(MainActivity.this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }


    }


    private void fetchMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void launchIntentBuilder(int requestcode) {

        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, requestcode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }

    @Override
    public void onMapLongClick(LatLng point) {
        mMap.addMarker(new MarkerOptions()
                .position(point)
                .title(point.toString())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        //update destination
        destination = new Position();
        destination.setLatitude(point.latitude);
        destination.setLongLatitude(point.longitude);
        passenger.setDistinationCoordinates(destination);

        Toast.makeText(getApplicationContext(),
                "New marker added", Toast.LENGTH_LONG)
                .show();
        String Resault = Util.Operations.getAddressName(point.latitude, point.longitude, this);
        place2tv.setText(Resault);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.PLACE_AUTOCOMPLETE_FROM) {

                Place mPlaceFrom = PlaceAutocomplete.getPlace(this, data);
                place1tv.setText(mPlaceFrom.getName());
                //remove current location marker if exist
                if (mCurrentMarker!=null) {
                    mCurrentMarker.remove();
                    mCurrentLocationIv.setImageResource(R.drawable.out);

                }

                source = new Position();
                source.setLatitude(mPlaceFrom.getLatLng().latitude);
                source.setLongLatitude(mPlaceFrom.getLatLng().longitude);
                passenger.setSourceCoordinates(source);
                putMarker(mPlaceFrom, Constants.PLACE_AUTOCOMPLETE_FROM);


            } else if (requestCode == Constants.PLACE_AUTOCOMPLETE_TO) {

                Place mPlaceTo = PlaceAutocomplete.getPlace(this, data);
                place2tv.setText(mPlaceTo.getName());
                destination = new Position();
                destination.setLatitude(mPlaceTo.getLatLng().latitude);
                destination.setLongLatitude(mPlaceTo.getLatLng().longitude);
                passenger.setDistinationCoordinates(destination);
                putMarker(mPlaceTo, Constants.PLACE_AUTOCOMPLETE_TO);

            }
        }

    }


    private void putMarker(Place place, int mCheckTripType) {
        LatLng pos = place.getLatLng();

        if (mCheckTripType == Constants.PLACE_AUTOCOMPLETE_FROM) {

            markerFrom = mMap.addMarker(new MarkerOptions()
                    .position(pos)
                    .title(place.getName().toString()));
            markerFrom.setTitle(place.getName().toString());
            markerFrom.setTag(1);
        } else if (mCheckTripType == Constants.PLACE_AUTOCOMPLETE_TO) {
            markerTo = mMap.addMarker(new MarkerOptions()
                    .position(pos)
                    .title(place.getName().toString()));
            markerTo.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            markerTo.setTitle(place.getName().toString());
            markerTo.setTag(2);

        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, Constants.DEFAULT_ZOOM));

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLongClickListener(this);

    }


    @Override
    public void onLocationChanged(Location location) {
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            mCurrentLatitude = mLastLocation.getLatitude();
            mCurrentLongLatitude = mLastLocation.getLongitude();
            mCurrentLocationPos.setLatitude(mCurrentLatitude);
            mCurrentLocationPos.setLongLatitude(mCurrentLongLatitude);
            passenger.setCurrentCoordinates(mCurrentLocationPos);

            try {
                String Resault = Util.Operations.getAddressName(mCurrentLatitude, mCurrentLongLatitude, this);

                place1tv.setText(Resault);
                LatLng CurLatLang = new LatLng(mCurrentLatitude, mCurrentLongLatitude);
                markerFrom = mMap.addMarker(new MarkerOptions()
                        .position(CurLatLang)
                        .title(Resault));

            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();

            }


        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void drawingRoute(View view) {
        start = passenger.getmSource();
        end = passenger.getmDistination();
        if (Util.Operations.isOnline(this)) {
            if (start == null || end == null) {
                if (source == null) {
                    Toast.makeText(this, getString(R.string.starting_point_error), Toast.LENGTH_SHORT).show();
                }
                if (destination == null) {

                    Toast.makeText(this, getString(R.string.end_point_error), Toast.LENGTH_SHORT).show();
                }
            } else {
                progressDialog = ProgressDialog.show(this, "Please wait.",
                        "Fetching route information.", true);
                Routing routing = new Routing.Builder()
                        .travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(this)
                        .alternativeRoutes(true)
                        .waypoints(start, end)
                        .build();
                routing.execute();
                mMap.setTrafficEnabled(true);

            }
        } else {
            Toast.makeText(this, "Please Check Internt Connection.", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onRoutingFailure(RouteException e) {
        progressDialog.dismiss();
        if (e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        progressDialog.dismiss();
        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);
        mMap.moveCamera(center);
        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }
        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {
            //In case of more than 5 alternative routes
            int colorIndex = i % Constants.COLORS.length;
            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(ContextCompat.getColor(this, Constants.COLORS[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);
            Toast.makeText(getApplicationContext(), "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingCancelled() {
    }
}

