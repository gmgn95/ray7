package com.mahmoud.mohammed.rayhtask.utils;

import android.app.Application;

import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;

/**
 * Created by siko on 6/10/2017.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        new Instabug.Builder(this, "7768139db44584ea2728f5421184b0ec")
                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
                .build();

    }
}
