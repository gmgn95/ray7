package com.mahmoud.mohammed.rayhtask.utils;

import com.mahmoud.mohammed.rayhtask.R;

/**
 * Created by siko on 6/10/2017.
 */

public class Constants {
    public static final int PLACE_AUTOCOMPLETE_FROM = 1;
    public static final int PLACE_AUTOCOMPLETE_TO = 2;
    public static final int DEFAULT_ZOOM = 14;
    public static final String GOOGLE_DIRICTIONS_API_KEY ="AIzaSyAc7pVcI_REyRV-8KTcIMmHt8NnXIO_tw8";
    public static final int[] COLORS = new int[]{
            R.color.colorPrimary,R.color.colorPrimaryDark,R.color.colorAccent,R.color.dark_color,R.color.primary_dark_material_light

    };

}
