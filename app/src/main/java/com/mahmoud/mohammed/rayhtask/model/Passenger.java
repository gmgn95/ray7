package com.mahmoud.mohammed.rayhtask.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by siko on 6/10/2017.
 */

public class Passenger implements Parcelable {
   private LatLng mSource;
    private String ID;
    private Trip trip;
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public Passenger() {
    }

    public LatLng getmDistination() {
        return mDistination;
    }

    public LatLng getmCurrentLocation() {
        return mCurrentLocation;
    }

    private LatLng mDistination;

    public LatLng getmSource() {
        return mSource;
    }


    private LatLng mCurrentLocation;

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip)
    {
       this.trip=trip;
    }

public void setSourceCoordinates(Position position)
{
    mSource=new LatLng(position.getLatitude(),position.getLongLatitude());

}
    public void setCurrentCoordinates(Position position)
    {
        mCurrentLocation=new LatLng(position.getLatitude(),position.getLongLatitude());

    }



    public void setDistinationCoordinates(Position position)
    {
        mDistination=new LatLng(position.getLatitude(),position.getLongLatitude());

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mSource, flags);
        dest.writeParcelable(this.mDistination, flags);
        dest.writeParcelable(this.mCurrentLocation, flags);
    }

    protected Passenger(Parcel in) {
        this.mSource = in.readParcelable(LatLng.class.getClassLoader());
        this.mDistination = in.readParcelable(LatLng.class.getClassLoader());
        this.mCurrentLocation = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Parcelable.Creator<Passenger> CREATOR = new Parcelable.Creator<Passenger>() {
        @Override
        public Passenger createFromParcel(Parcel source) {
            return new Passenger(source);
        }

        @Override
        public Passenger[] newArray(int size) {
            return new Passenger[size];
        }
    };
}
