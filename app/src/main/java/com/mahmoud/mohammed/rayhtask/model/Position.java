package com.mahmoud.mohammed.rayhtask.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by siko on 6/10/2017.
 */

public class Position {
    private double Latitude;
    private double LongLatitude;


    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongLatitude() {
        return LongLatitude;
    }

    public void setLongLatitude(double longLatitude) {
        LongLatitude = longLatitude;
    }
}
